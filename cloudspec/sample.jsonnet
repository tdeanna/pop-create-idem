{
    "project_name": "",
    "api_version": "latest",
    "service_name": "",
    "request_format": "",
    "plugins": {
        "service_name.sub.plugin_name": {
            "imports": ["import pathlib", "from sys import executable"],
            "virtual_imports": [],
            "func_alias": {"list_":  "list"},
            "virtualname": "",
            "doc": "a module level docstring",
            "functions":{
                "function_name": {
                    "doc": "a function docstring",
                    "hardcoded": {},
                    "return_type": "int",
                    "params":{
                        # Simple int param
                        "param_name_simple": {
                            "doc": "a parameter docstring",
                            "param_type": "int",
                            "required": "True",
                            "default": "None",
                            "target": "query_params",
                            "target_type": "mapping"
                        }
                        # A complex nested argument to be represented as python dataclass.
                        # Complex argument can be a standalone dataclass as well as dataclass within a Dict or a List.
                        # Multiple levels of nesting is supported.
                        "param_name_complex": {
                            "doc": "a parameter docstring",
                            # param_type should be {}|List[{}]|Dict[str, {}]
                            "param_type": "{}|List[{}]|Dict[str, {}]",
                            "required": "True",
                            "default": "None",
                            "target": "kwargs",
                            "target_type": "mapping"
                            "member": {
                                "member_param_1": {
                                    "doc": "a parameter docstring",
                                    "param_type": "int",
                                    "required": "True",
                                    "default": "None",
                                    "target": "kwargs",
                                    "target_type": "mapping"
                                }
                                "member_param_2": {
                                    "doc": "a parameter docstring",
                                    "param_type": "str",
                                    "required": "False",
                                    "default": "None",
                                    "target": "kwargs",
                                    "target_type": "mapping"
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}
