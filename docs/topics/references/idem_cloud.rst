==========
Idem Cloud
==========

The advantage to idem-cloud is that you can have a standardized CLI interface for every cloud.
With `acct` profiles, credentials for accessing your cloud are encrypted --
no more plaintext credentials on your file system or in your shell history!
`idem-cloud` states work together seamlessly between cloud providers and profiles;
Your instance from one cloud provider can depend on the existence of a vpc from a completely different cloud provider.
In this document we will explore the structure of an idem-cloud project so that you can easily create your own!


Authenticating with idem-cloud
==============================

No matter what you use for your authentication method,
in the end they all need to be interchangeable when we are calling exec modules.
For example, let's say I want all my plugins to authenticate with aiohttp,
but I want one plugin to handle username and password authentication,
and I want to make another plugin that handles authentication with an api token.
Under the hood they're both creating an aiohttp session, but they're using credentials
in different ways.

Or maybe we want to have an account plugin that authenticates with a cookie, and that's
a completely different way of using an aiohttp session, but on the back end it's the same,
and it will be the same to exec modules, that are calling the api methods. You could use
a plain request library or httpx, or something else. Account plugins is where you can
handle all those differences, and have different ways of authenticating.

That's the reason you'd want different account plugins;
for different ways of authenticating to the same service.

This is an example of what is looks like;
This is the file name:

credentials.yaml

And these are the contents of the file:

.. code-block:: sls

    {simple_service_name}.basic_auth:
      default:
        username: my_user
        password: my_pass

Now encrypt the credentials file and add the encryption key and encrypted file path
to the ENVIRONMENT.

This is an example of how to encrypt the credential file.

.. code:: bash

    idem encrypt credentials.yaml

output::

    -A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI=

This is how you add these to your environment;
Use a variable assigned to the value of the output,
and second variable assigned to value of the path to the encrypted fernet file.

.. code:: bash

    export ACCT_KEY="-A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet

Idem will now be able to read these credentials for authentication to your cloud!

Exec modules
============

functions placed in `{project_name}/exec/{simple_service_name}/` will appear on the hub under
`hub.exec.{simple_service_name}.*`.  The directory structure under `exec` is arbitrary to idem, so use it to keep your
functions organized.  Do NOT put all your functions in one giant file.  That is not very pop.

The directory structure affects where functions are placed on the hub, and how they are referenced on the CLI.

If you create a function called "get" in `{project_name}/exec/{simple_service_name}/instance`,
it can be called from the hub within code like so:

.. code-block:: python

    hub.exec.simple_service_name.sample.get(name="sample_name", resource_id="resource_1")

It could be called from the idem cli like this:

.. code-block:: bash

    idem exec {simple_service_name}.sample.get name=sample_name resource_id=resource_1

The profile you want to use from your encrypted credentials file can be specified on the command line when calling an exec module directly.
The default is to use the profile named "default".

.. code:: bash

    idem exec --acct-profile my-staging-env {simple_service_name}.sample.list

State modules
=============

Example of using a sample state in SLS:

.. code:: sls

    ensure_sample_exists:
      {simple_service_name}.sample.present:
        - name: a_sample_name
        - description: Managed by Idem

Create sample state:

.. code:: bash

    idem state my_state.sls

Delete sample state:

.. code:: bash

    idem state my_state.sls --invert


Keep popping! Enjoy!
